# Azure Provider
provider "azurerm" {
  version = "=2.0.0"
  features {}
}

# State Backend
terraform {
  backend "azurerm" {
    resource_group_name  = "MyFirstVM-State"
    storage_account_name = "myfirstvmstate"
    container_name       = "terraform-state"
    key                  = "myfirstvm.terraform.tfstate"
  }
}